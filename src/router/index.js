import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import Movies from '../views/Movies.vue';
import LiveTv from '../views/LiveTv.vue';
import TvShows from '../views/TvShows.vue';
import Music from '../views/Music.vue';
import Watchlist from '../views/Watchlist.vue';
import Settings from '../components/Settings.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/movies',
    name: 'Movies',
    component: Movies,
  },
  {
    path: '/live-tv',
    name: 'Live-Tv',
    component: LiveTv,
  },
  {
    path: '/music',
    name: 'Music',
    component: Music,
  },
  {
    path: '/tv-shows',
    name: 'TV-Shows',
    component: TvShows,
  },
  {
    path: '/watchlist',
    name: 'WatchList',
    component: Watchlist,
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
